package launcher;

import com.google.cloud.ServiceOptions;
import com.google.pubsub.v1.ReceivedMessage;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
* Created by dmeeus1 on 7-6-2018
*/
public class Server {
    private static final Server SERVER = new Server();

    private static final String topicId = "uzleuven";
    private static final String projectId = ServiceOptions.getDefaultProjectId();
    private static final String subscriptionId = "demo";
    private Server(){
        try {
            System.out.println("Trying to set up server on 0::1:8090...");
            setupServer();
            System.out.println("Server running!");
        } catch (IOException ioex ) {
            ioex.printStackTrace();
        }
    }

    public static Server get(){
        return SERVER;
    }

    private void setupServer() throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress("uzlvddmeeus1.uz.kuleuven.ac.be", 8080), 8080);
        server.createContext("/subscribe", new SubscribeHandler());
        server.createContext("/publish", new PublishHandler());
        server.setExecutor(null);
        server.start();
    }

    static class PublishHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            System.out.println("Handling publish event..");
            InputStream requestBody = httpExchange.getRequestBody();
            System.out.println(requestBody);
            httpExchange.sendResponseHeaders(200, "success".length());

            // read the data
            byte[] data = new byte[Integer.parseInt(httpExchange.getRequestHeaders().getFirst("Content-length"))];
            requestBody.read(data);
            try {
                Pubsub.sendMessage(projectId, topicId, Arrays.asList(new String(data)));
            } catch (Exception e) {
                e.printStackTrace();
            }


            OutputStream os = httpExchange.getResponseBody();
            os.write("success".getBytes());
            os.flush();
            os.close();
        }
    }

    static class SubscribeHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            System.out.println("handling subscribe event");
            try {
                List<ReceivedMessage> subscriberWithSyncPull = Pubsub.createSubscriberWithSyncPull(projectId, subscriptionId, 10);
                String subOutput = subscriberWithSyncPull.stream()
                        .map(ReceivedMessage::getMessage)
                        .map(msg -> msg.getData().toStringUtf8())
                        .collect(Collectors.joining("\n"));
                System.out.printf("Subcriber data: %s\n", subOutput);
                httpExchange.sendResponseHeaders(200, subOutput.length());
                OutputStream os = httpExchange.getResponseBody();
                os.write(subOutput.getBytes());
                os.flush();
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
