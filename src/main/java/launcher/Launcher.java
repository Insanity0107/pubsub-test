package launcher;


import com.google.cloud.ServiceOptions;
import com.google.pubsub.v1.ReceivedMessage;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/*
* Created by dmeeus1 on 30-5-2018
*/
public class Launcher {

    private String topicId = "uzleuven";
    private String projectId = ServiceOptions.getDefaultProjectId();
    private String subscription = "demo";

    public static void main(String[] args) {
        Server s = Server.get();
        Launcher l = new Launcher();
    }

    public Launcher() {
        Scanner s = new Scanner(System.in);
        String line = "";
        System.out.println("publish / subscribe / stop");
        while (s.hasNextLine() && !(line = s.nextLine()).equals("stop")) {

            String command = line.split(" ")[0];
            switch (command) {
                case "publish":
                    try {
                        System.out.println("Sending data..");
                        Pubsub.sendMessage(projectId, topicId, Arrays.asList(line.substring(command.length()+1))); //+1 to avoid the space
                        System.out.println("Sent!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "subscribe":
                    try {
                        final List<ReceivedMessage> messages = Pubsub.createSubscriberWithSyncPull(projectId, subscription, 10);
                        Pubsub.processMessages(messages);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

}
