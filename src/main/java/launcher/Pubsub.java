package launcher;

import com.google.api.core.ApiFuture;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.cloud.pubsub.v1.stub.GrpcSubscriberStub;
import com.google.cloud.pubsub.v1.stub.SubscriberStub;
import com.google.cloud.pubsub.v1.stub.SubscriberStubSettings;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/*
* Created by dmeeus1 on 31-5-2018
*/
public interface Pubsub {

    /**
     * Send a message to the pubsub cloud service.
     *
     * @param projectId
     * @param topicId
     * @throws Exception
     */
    public static void sendMessage(final String projectId,
                                   final String topicId,
                                   final List<String> messages) throws Exception {
        final ProjectTopicName topicName = ProjectTopicName.of(projectId, topicId);
        final List<ApiFuture<String>> messageIdFutures = new ArrayList<>();

        Optional<Publisher> publisher = Optional.empty();
        try {
            publisher = Optional.of(Publisher.newBuilder(topicName).build());
            // schedule publishing one message at a time : messages get automatically batched
            List<PubsubMessage> pubsubMessages = messages.stream()
                    .map(ByteString::copyFromUtf8)
                    .map(bs -> PubsubMessage.newBuilder().setData(bs).build())
                    .collect(Collectors.toList());
            for (final PubsubMessage psMessage : pubsubMessages) {
                // Once published, returns a server-assigned message id (unique within the topic)
                ApiFuture<String> messageIdFuture = publisher.get().publish(psMessage);
                messageIdFutures.add(messageIdFuture);
            }
        }
        finally {
            publisher.ifPresent(pub -> {
                try {
                    pub.shutdown();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });

        }
    }

    /**
     * Recieve messages from the pubsub cloud service (synchronously)
     * @param projectId
     * @param subscriptionId
     * @param numOfMessages
     * @return
     * @throws Exception
     */
    public static List<ReceivedMessage> createSubscriberWithSyncPull(
            final String projectId,
            final String subscriptionId,
            int numOfMessages) throws Exception {

        final SubscriberStubSettings subscriberStubSettings = SubscriberStubSettings.newBuilder().build();
        final String subscriptionName = ProjectSubscriptionName.format(projectId, subscriptionId);

        try (SubscriberStub subscriber = GrpcSubscriberStub.create(subscriberStubSettings)) {
            final PullRequest pullRequest = PullRequest.newBuilder()
                    .setMaxMessages(numOfMessages)
                    .setReturnImmediately(false) // return immediately if messages are not available
                    .setSubscription(subscriptionName)
                    .build();

            final PullResponse pullResponse = subscriber.pullCallable().call(pullRequest);
            final List<String> ackIds = pullResponse.getReceivedMessagesList().stream()
                    .map(pm -> pm.getAckId())
                    .collect(Collectors.toList());

            if (ackIds.isEmpty()) {
                return Collections.emptyList();
            }
            // acknowledge received messages
            final AcknowledgeRequest acknowledgeRequest = AcknowledgeRequest.newBuilder()
                    .setSubscription(subscriptionName)
                    .addAllAckIds(ackIds)
                    .build();

            // use acknowledgeCallable().futureCall to asynchronously perform this operation
            subscriber.acknowledgeCallable().call(acknowledgeRequest);
            return pullResponse.getReceivedMessagesList();
        }
    }

    public static void processMessages(final List<ReceivedMessage> messages) {
        if (messages.isEmpty()) {
            System.out.println("No messages in queue");
            return;
        }

        messages.stream()
                .map(ReceivedMessage::getMessage)
                .map(msg -> msg.getData().toStringUtf8())
                .forEach(data -> System.out.println(data));
    }
}
